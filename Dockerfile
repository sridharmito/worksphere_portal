FROM registry.gitlab.lab3dvlp.com/docker/nginx-angular:1.14
MAINTAINER Felipe Weckx <felipe@lab3dvlp.com>

# Copy app
COPY dist /var/www
