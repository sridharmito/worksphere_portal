// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    url: 'http://202.61.120.46:8090',
    grant_type: 'password',
    client_id: 'b2news-android',
    client_secret: 'bC7MJ5zr7ZE5',
    company_id: 1,
    saml_enabled: false
};
