import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AuthGuard} from '../shared/guards/auth.guard';
import {HomeComponent} from '../home/home.component';
import {LayoutComponent} from './layout.component';
import {MenuResolveService} from '../home/services/menu-resolve.service';
import {TermsComponent} from '../home/terms/terms.component';
import {TermsResolveService} from '../home/terms/services/terms-resolve.service';
import {AboutComponent} from '../home/about/about.component';
import {PageComponent} from '../page/page.component';


const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    {
        path: '',
        component: LayoutComponent,
        resolve: { menu: MenuResolveService },
        canActivate: [ AuthGuard ],
        children: [
            {
                path: 'home',
                component: HomeComponent,
                data: { title: 'Feed' }
            },
            {
                path: 'page',
                loadChildren: 'app/page/page.module#PageModule'
            },
            {
                path: 'accept-terms',
                component: TermsComponent,
                resolve: { terms: TermsResolveService },
                data : { accept: true }
            },
            {
                path: 'terms',
                component: TermsComponent,
                resolve: { terms: TermsResolveService },
                data : { accept: false }
            },
            {
                path: 'about',
                component: AboutComponent,
                resolve: { terms: TermsResolveService },
                data : { accept: false }
            },
            {
                path: 'perfil',
                loadChildren: 'app/perfil/perfil.module#PerfilModule'
            },
            {
                path: 'calendar',
                loadChildren: 'app/calendar/calendar.module#CalendarModule'
            },
            {
                path: 'news',
                loadChildren: 'app/news/news.module#NewsModule'
            },
            {
                path: 'resource',
                loadChildren: 'app/resource/resource.module#ResourceModule'
            },
            {
                path: 'contactus',
                loadChildren: 'app/contactus/contactus.module#ContactusModule'
            }
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LayoutRoutingModule { }
