import { Component, Input, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TitleService } from '../../shared/services/title.service';
import { NewsService } from '../../news/service/news.service';
import { GeneralServiceService } from '../../shared/services/general-service.service';
import { LoginService } from '../../login/services/login.service';
import { ContactListService } from '../../contactlist/contactlist.service';
import { ResourceService } from '../../resource/services/resource.service';
import { SessionService } from '../../utils/session.service';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';


@Component({
    selector: 'app-menu-bar',
    templateUrl: './menu-bar.component.html',
    styleUrls: ['./menu-bar.component.css'],
    providers: [SessionService,ContactListService]
})

export class MenuBarComponent implements OnInit {


    @Input()
    logoUrl;
    searchEnabled = false;
    notifications: Array<Object>;
    unreadCount = 0;
    showSubTitle: boolean;
    subTitle: string;
    url: string;
    title: string;
    user: any;
    token: string;
    userInfo: any;
    mainMenu: any;
    notificationOpen = false;
    currentMenu = 'home';
    currentSubCategory: string = null;
    overallPosts: any;
    overallSearch: any;
    searchText: any;
    searchEnd: any = 3;
    showContPop = false;
    contact: any;
    showContList: any = false;
    contactList: any;
    alphabets: any;

    constructor(

        private newsService: NewsService,
        private titleService: TitleService,
        private loginService: LoginService,
        private contactListService: ContactListService,
        private resourceService: ResourceService,
        private generalService: GeneralServiceService,
        private router: Router,
        private _scrollToService: ScrollToService,
        private sessionService: SessionService,
        private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
        this.overallPosts = [];

        this.titleService.newTitle.subscribe(title => {
            this.title = title.name;
            this.subTitle = title.subTitle;
            this.url = title.url;
            this.showSubTitle = title.showSubTitle;
        });
        this.overallSearch = [];

        this.refreshNotifications();
        setInterval(() => {
            this.refreshNotifications();
            this.updateUnread();
        }, 60000);

        this.userInfo = JSON.parse(localStorage.getItem('user'));
        this.mainMenu = JSON.parse(localStorage.getItem('menu'));
        this.token = localStorage.getItem('access_token');

        this.loginService.getUserInfo()
            .subscribe(
                user => this.userInfo = user
            );

        this.contactListService.fetchAllContacts()
            .subscribe(
                data => {
                    this.contactList = data._embedded.user;
                }
            );
        this.alphabets = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    }

    sliceList(id) {
        const config: ScrollToConfigOptions = {
            target: 'index_' + id
        };
        this._scrollToService.scrollTo(config);
    }

    brazil() {
        this.sessionService.setLocale('pt-BR');
        window.location.reload();
    }

    usenglish() {
        this.sessionService.setLocale('en-US');
        window.location.reload();
    }
    spain() {
        this.sessionService.setLocale('es');
        window.location.reload();
    }


    showContacts(contact) {
        this.contact = contact;
        this.showContPop = true;
    }

    searchOverallData(searchText) {
        this.overallSearch = [];
        if (!this.overallPosts || !searchText) {
            this.overallSearch = [];
        } else {
            searchText = searchText.toLowerCase();
            this.newsService.getOverallSearch(searchText)
                .subscribe(searchData => {
                    let dataList = searchData._embedded.list;
                    this.overallSearch = [];
                    dataList.forEach(element => {
                        for (var key in element) {
                            if (element[key].length > 0) {
                                var obj = { categoryName: key.toLocaleUpperCase(), data: element[key], endLimit: 3 };
                                this.overallSearch.push(obj);
                            }
                        }
                    });
                });
        }
    }

    searchRouting(key, data) {
        var routeKey = "";
        var id = "";
        key = key.toLowerCase();
        if (key == 'news') {
            routeKey = 'news';
            id = data.id;
        } else if (key == 'users') {
            this.showContacts(data);
        } else if (key == 'notification') {
            routeKey = 'news';
            id = data.description.news_id;
        } else if (key == 'formPost') {
            routeKey = 'news';
            id = data.id;
        } else if (key == 'message') {
            routeKey = 'news';
            id = data.id;
        } else if (key == 'event') {
            routeKey = 'calendar';
            id = data.id;
        }
        this.router.navigate(['/' + routeKey + '/' + id]);
    }

    refreshNotifications() {
        this.newsService.getNotifications()
            .subscribe(notifications => {
                this.notifications = notifications._embedded.list;
            });
    }

    updateUnread() {
        // Check user info
        this.loginService.getUserInfo()
            .subscribe(
                user => {
                    localStorage.setItem('user', JSON.stringify(user));
                    this.unreadCount = user.unread_count;
                    if (user.should_accept_terms) {
                        this.router.navigate(['/accept-terms']);
                    }
                }
            );
    }
    clearSearch() {
        this.searchEnabled = false;
        this.overallSearch = [];
        this.searchText = '';
    }

    openMenu(name: string) {
        this.currentMenu = name;
    }

    toggleSubCategory(id: string) {
        this.currentSubCategory = id;
        return false;
    }
}
