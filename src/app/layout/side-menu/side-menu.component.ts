import { Component, OnInit, AfterViewInit } from '@angular/core';
import {LoginService} from '../../login/services/login.service';

declare var $: any;
@Component({
    selector: 'app-side-menu',
    templateUrl: './side-menu.component.html',
    styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent implements OnInit, AfterViewInit {
    token: string;
    userInfo: any;
    mainMenu: any;

    constructor(private loginService: LoginService) { }

    ngOnInit() {
        this.userInfo = JSON.parse(localStorage.getItem('user'));
        this.mainMenu = JSON.parse(localStorage.getItem('menu'));
        this.token = localStorage.getItem('access_token');
        
        this.loginService.getUserInfo()
            .subscribe(
                user => this.userInfo = user
            );

    }

    ngAfterViewInit() {
	    $('.collapse')
	        .on('shown.bs.collapse',  function() {
	            $(this)
	                .prev()
	                .find(".icon-triangle")
	                .addClass("icon-triangle-90");
	        })
	        .on('hidden.bs.collapse',  function() {
	            $(this)
	                .prev()
	                .find(".icon-triangle-90")
	                .removeClass("icon-triangle-90");
	        });

        $(".menu__navigation").click(function(){
            $(".navigation").toggleClass("navigation--open");
            window.scrollTo(0, 0);
        });

        $(".menu__notification").click(function(){
            $(".menu__notification").toggleClass("menu__notification--open");
            $(".notification").toggleClass("notification--open");
            window.scrollTo(0, 0);
        });

        $(".menu__search-link").click(function(){
            $(".menu__search").toggleClass("menu__search--open");
        });
        $(".menu__search-close").click(function(){
            $(".menu__search").toggleClass("menu__search--open");
        });

    }

}
