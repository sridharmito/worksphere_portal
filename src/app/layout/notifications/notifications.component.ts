import {Component, Input} from '@angular/core';
import {NewsService} from '../../news/service/news.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-notifications',
    templateUrl: './notifications.component.html',
    styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent {
    @Input()
    notifications: Array<any>;

    @Input()
    show = false;

    constructor(private router: Router, private newsService: NewsService) {
    }

    openNotification(notification) {
        notification.read_date = true;
        this.newsService.setNotificationRead(notification.notificationuser_id)
            .subscribe(
                () => {  }
            );

        if (notification.type === 'news') {
            this.router.navigate(['/news', notification.description.news_id]);
        } else if (notification.type === 'event') {
            this.router.navigate(['/event', notification.description.event_id]);
        } else if (notification.type === 'message') {
            this.router.navigate(['/contactus/details', notification.description.message_id]);
        }

        this.show = false;
    }

    markAllRead() {
        this.newsService.setNotificationsAsRead()
            .subscribe(
                res => {
                    for (const notification of this.notifications) {
                        notification.read_date = true;
                    }
                }
            );
    }
}
