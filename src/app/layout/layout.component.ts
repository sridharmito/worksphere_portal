import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login/services/login.service';
import { ActivatedRoute } from '@angular/router';
import { CompanyService } from '../shared/services/company.service';

declare var $: any;

@Component({

    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
    static showFooter = true;
    headerLogoUrl: string;
    footerLogoUrl: string;
    title: any = 'Feed';


    constructor(private service: LoginService, private companyService: CompanyService, private route: ActivatedRoute) { }


    ngOnInit() {

        this.route.data.subscribe(data => localStorage.setItem('menu', JSON.stringify(data['menu'])));

        const hostname = window.location.hostname;
        let slug: string;
        if (hostname === 'localhost' || hostname === 'portal.worksphere.com.br') {
            slug = 'b2mdia';
        } else {
            slug = hostname.split('.')[0];
        }

        this.companyService.getCompanyConfig(slug)
            .subscribe(
                (res) => {
                    this.headerLogoUrl = res.portal.url_logo_header;
                    this.footerLogoUrl = res.portal.url_logo_footer;
                }
            );

    }

    closeMenu() {
        $('.navigation').removeClass('navigation--open');
    }
}
