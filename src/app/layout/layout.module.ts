import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelMenuModule, MenuItem, DropdownModule } from 'primeng/primeng';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FileUploadModule, GrowlModule } from 'primeng/primeng';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { LayoutComponent } from './layout.component';
import { FooterComponent } from './footer/footer.component';
import { MenuBarComponent } from './menu-bar/menu-bar.component';
import { LayoutRoutingModule } from './layout.routing';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { DirectivesModule } from '../shared/directives/directives.module';
import { TitleService } from '../shared/services/title.service';
import { NotificationResolve } from '../home/services/notifications-resolve.service';
import { NotificationsComponent } from './notifications/notifications.component';
import { NewsService } from '../news/service/news.service';
import { GeneralServiceService } from '../shared/services/general-service.service';
import { TermsResolveService } from '../home/terms/services/terms-resolve.service';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { FilterPipe } from 'app/utils/FilterPipe';
import { OrderrByPipe } from 'app/utils/OrderByPipe';


@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        PanelMenuModule,
        DirectivesModule,
        ReactiveFormsModule,
        DropdownModule,
        TabsModule,
        FormsModule,
        FileUploadModule,
        GrowlModule,
        BsDropdownModule
    ],
    declarations: [
        LayoutComponent,
        FooterComponent,
        MenuBarComponent,
        SideMenuComponent,
        NotificationsComponent,
        FilterPipe,
        OrderrByPipe
    ],
    providers: [
        GeneralServiceService,
        NewsService,
        NotificationResolve,
        TermsResolveService,
        TitleService
    ]
})
export class LayoutModule { }
