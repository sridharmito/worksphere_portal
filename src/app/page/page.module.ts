import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PageRoutingModule} from './page.routing';
import {PageComponent} from './page.component';
import {PageService} from './service/page.service';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        PageRoutingModule,
        ReactiveFormsModule
    ],
    declarations: [
        PageComponent
    ],
    providers: [
        PageService
    ]
})
export class PageModule { }
