import { Component, OnInit } from '@angular/core';
import {PageService} from './service/page.service';
import {ActivatedRoute, ActivatedRouteSnapshot} from '@angular/router';
import {DomSanitizer} from "@angular/platform-browser";
import {LayoutComponent} from "../layout/layout.component";

@Component({
    selector: 'app-page',
    templateUrl: './page.component.html',
    styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {
    url: any;

    constructor(private pageService: PageService, private route: ActivatedRoute, private domSanitizer: DomSanitizer) {

    }

    ngOnInit(): void {
        this.route.params.subscribe(
            params => {
                LayoutComponent.showFooter = false;
                this.url = this.domSanitizer.bypassSecurityTrustResourceUrl(this.pageService.getPageUrl(this.route.snapshot.params['id']));
            }
        );
    }
}
