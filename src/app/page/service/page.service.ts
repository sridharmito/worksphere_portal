import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';

import {environment} from '../../../environments/environment';
import {HeadersService} from '../../shared/services/headers.service';
import {HandleErrorService} from '../../shared/services/handle-error.service';

@Injectable()
export class PageService {

    constructor(
        private http: Http,
        private handleError: HandleErrorService,
        private service: HeadersService) {
    }

    /**
     * Retorna a noticia com base no ID enviando.
     * @param {number} id ID da noticia
     */
    getPageUrl(slug: string) {
        return `${environment.url}/page/${environment.company_id}/${slug}?key=${this.service.token}`;
    }
}
