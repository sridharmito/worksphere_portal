import {Component, OnInit, Sanitizer} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GeneralServiceService } from '../../shared/services/general-service.service';
import {DomSanitizer, SafeHtml} from "@angular/platform-browser";
import {TitleService} from "../../shared/services/title.service";
import {LoginService} from "../../login/services/login.service";

@Component({
    selector: 'app-terms',
    templateUrl: './terms.component.html',
    styleUrls: ['./terms.component.css']
})
export class TermsComponent implements OnInit {

    terms: any;
    termsHtml: SafeHtml;
    accept: true;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private fb: FormBuilder,
        private generalService: GeneralServiceService,
        private titleService: TitleService,
        private loginService: LoginService,
        private sanitizer: DomSanitizer
    ) { }

    ngOnInit() {
        const mainMenu = JSON.parse(localStorage.getItem('menu'));
        this.titleService.setTitle({ name: mainMenu.terms_caption, showSubTitle: false, url: '/home' });

        this.route.data.subscribe(data => {
            this.terms = data['terms'];
            this.accept = data.accept;
            this.termsHtml = this.sanitizer.bypassSecurityTrustHtml(this.terms.text);
        });

        this.loginService.getUserInfo()
            .subscribe(
                user => {
                    if (!user.should_accept_terms && this.route.snapshot.url[0].path !== 'terms') {
                        this.router.navigate(['/home']);
                    }
                }
            );
    }

    acceptTerms(value?: any) {
        this.generalService.acceptTerms()
            .subscribe(() => {
                const user = JSON.parse(localStorage.getItem('user'));
                user.should_accept_terms = false;
                localStorage.setItem('user', JSON.stringify(user));
                this.router.navigate(['/home']);
            });
    }

    declineTerms() {
        localStorage.clear();
        this.router.navigate(['/login']);
    }

}
