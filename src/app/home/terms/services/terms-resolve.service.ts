import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { GeneralServiceService } from '../../../shared/services/general-service.service';

@Injectable()
export class TermsResolveService implements Resolve<any> {

    constructor(private service: GeneralServiceService) { }

    resolve() {
        const data = this.service.getTerms();
        return data;
    }

}
