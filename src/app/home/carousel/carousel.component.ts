import { Component, OnInit, Input } from '@angular/core';
import {NewsService} from "../../news/service/news.service";

@Component({
    selector: 'app-carousel',
    templateUrl: './carousel.component.html',
    styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {
    pages: Array<any> = [];

    @Input() set news(news: Array<Object>) {
        this.allNews = news;
        for (let i = 0; i < news.length; i += 3) {
            const page = {
                first: null,
                second: null,
                third: null
            };

            page.first = news[i];
            if (news[i + 1]) {
                page.second = news[i + 1];
            }

            if (news[i + 2]) {
                page.third = news[i + 2];
            }

            this.pages.push(page);
        }
    }

    get news() {
        return this.allNews;
    }

    allNews: Array<Object> = [];
    myInterval = 5000;
    noWrapSlides = false;

    constructor(private newsService: NewsService) { }

    ngOnInit() {

    }

    likeByNews(news: any) {
        news.spinner = true;
        if (news.liked) {
            this.newsService.likeDelCommentById(news['id'])
                .subscribe(like => {
                    news['likes_count']--;
                    news['liked'] = false;
                    news.spinner = false;
                });
        } else {
            this.newsService.likeAddCommentById(news['id'])
                .subscribe(like => {
                    news['likes_count']++
                    news['liked'] = true;
                    news.spinner = false;
                });
        }
    }
}
