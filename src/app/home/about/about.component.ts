import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {GeneralServiceService} from '../../shared/services/general-service.service';
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';
import {TitleService} from "../../shared/services/title.service";

@Component({
    selector: 'app-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

    about: any;
    aboutHtml: SafeHtml;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private generalService: GeneralServiceService,
                private titleService: TitleService,
                private sanitizer: DomSanitizer) {
    }

    ngOnInit() {
        const mainMenu = JSON.parse(localStorage.getItem('menu'));
        this.titleService.setTitle({ name: mainMenu.about_caption, showSubTitle: false, url: '/home' });

        this.generalService.getAbout()
            .subscribe(data => {
                this.about = data.about;
                this.aboutHtml = this.sanitizer.bypassSecurityTrustHtml(this.about);
            });
    }
}
