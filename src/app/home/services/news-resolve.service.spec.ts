import { TestBed, inject } from '@angular/core/testing';

import { HomeNewsResolveService } from './news-resolve.service';

describe('HomeNewsResolveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HomeNewsResolveService]
    });
  });

  it('should ...', inject([HomeNewsResolveService], (service: HomeNewsResolveService) => {
    expect(service).toBeTruthy();
  }));
});
