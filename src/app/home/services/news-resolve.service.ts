import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { NewsService } from '../../news/service/news.service';

@Injectable()
export class HomeNewsResolveService implements Resolve<any> {

    constructor(private newsService: NewsService) { }

    /**
     * A home irá aparecer somente após ter os dados retornados da api.
     */
    resolve() {
        return this.newsService.getNews(1);
    }

}
