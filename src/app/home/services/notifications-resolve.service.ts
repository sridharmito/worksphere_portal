import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { NewsService } from '../../news/service/news.service';

@Injectable()
export class NotificationResolve implements Resolve<any> {

    constructor(private newsService: NewsService) { }

    resolve() {
        return this.newsService.getNotifications();
    }
}
