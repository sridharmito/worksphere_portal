import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { MenuService } from '../../shared/services/menu.service';

@Injectable()
export class MenuResolveService implements Resolve<any> {

    constructor(private menuService: MenuService) { }

    resolve() {
        return this.menuService.getMenu();
    }
}
