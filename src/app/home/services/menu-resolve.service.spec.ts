import { TestBed, inject } from '@angular/core/testing';

import { MenuResolveService } from './menu-resolve.service';

describe('MenuResolveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MenuResolveService]
    });
  });

  it('should ...', inject([MenuResolveService], (service: MenuResolveService) => {
    expect(service).toBeTruthy();
  }));
});
