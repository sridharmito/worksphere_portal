import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { TitleService } from '../shared/services/title.service';
import { NewsService } from '../news/service/news.service';
import {LoginService} from "../login/services/login.service";
import {CompanyService} from "../shared/services/company.service";
import {DomSanitizer} from "@angular/platform-browser";

declare var $: any;

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    sliderNews: Array<any> = [];
    latestNews: Array<any> = [];
    popularNews: Array<any> = [];
    menu: Object;
    portal: any =  {};

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private newsService: NewsService,
        private titleService: TitleService,
        private loginService: LoginService,
        private companyService: CompanyService,
        private sanitizer: DomSanitizer) { }

    ngOnInit() {
        this.titleService.setTitle({ name: 'Feed', showSubTitle: false });

        // Check user info
        this.loginService.getUserInfo()
            .subscribe(
                user => {
                    localStorage.setItem('user', JSON.stringify(user));
                    if (user.should_accept_terms) {
                        this.router.navigate(['/accept-terms']);
                    }
                }
            );

        // Get portal settings
        this.companyService.getPortalConfig()
            .subscribe(
                portal => {
                    this.portal = portal;
                }
            );

        // Fetch slider news
        this.newsService.fetchSliderNews(9)
            .subscribe(
                data => {
                    this.sliderNews = data._embedded.list;
                }
            );

        // Fetch slider news
        this.newsService.fetchLatestNews(5)
            .subscribe(
                data => {
                    this.latestNews = data._embedded.list;
                }
            );

        // Fetch slider news
        this.newsService.fetchPopularNews(11)
            .subscribe(
                data => {
                    this.popularNews = data._embedded.list;
                }
            );

    }

    sanitize(html: string) {
        return this.sanitizer.bypassSecurityTrustHtml(html);
    }

    likeByNews(news: any) {
        news.spinner = true;
        if (news.liked) {
            this.newsService.likeDelCommentById(news['id'])
                .subscribe(like => {
                    news['likes_count']--;
                    news['liked'] = false;
                    news.spinner = false;
                });
        } else {
            this.newsService.likeAddCommentById(news['id'])
                .subscribe(like => {
                    news['likes_count']++
                    news['liked'] = true;
                    news.spinner = false;
                });
        }
    }
}
