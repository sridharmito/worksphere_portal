import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as moment from 'moment';
import {ResourceService} from '../services/resource.service';
import {TitleService} from '../../shared/services/title.service';

declare var iziToast: any;

@Component({
    selector: 'app-my-reservations',
    templateUrl: './my-reservations.component.html',
    styleUrls: ['./my-reservations.component.css']
})
export class MyReservationsComponent implements OnInit {
    reservations: any[] = [];

    constructor(
        private route: ActivatedRoute,
        private resourceService: ResourceService,
        private router: Router,
        private titleService: TitleService) {

    }

    ngOnInit() {
        this.titleService.setTitle({name: 'Minhas Reservas', showSubTitle: false, url: '/home'});
        this.loadReservations();
    }

    loadReservations() {
        const minDate = moment().format('YYYY-MM-DD');
        this.resourceService.fetchMyReservations(minDate).subscribe(
            reservations => {
                this.reservations = reservations._embedded.list;
            }
        )
    }

    confirmCancel(reservation: any) {
        if (confirm('Tem certeza que deseja cancelar a reserva?')) {
            this.resourceService.deleteReservation(reservation.resource_id, reservation.id).subscribe(
                success => {
                    iziToast.success({
                        title: 'Sucesso',
                        message: 'Reserva cancelada',
                        color: 'white',
                        icon: 'glyphicon glyphicon-ok',
                        iconColor: 'green',
                        layout: 2,
                        balloon: true,
                        position: 'bottomCenter',
                        pauseOnHover: false,
                    });
                    this.loadReservations();
                }, error => {
                    iziToast.success({
                        title: 'Erro',
                        message: 'Ocorreu um erro. Tente novamente mais tarde',
                        color: 'white',
                        icon: 'glyphicon glyphicon-remove',
                        iconColor: 'red',
                        layout: 2,
                        balloon: true,
                        position: 'bottomCenter',
                        pauseOnHover: false,
                    });
                }
            )
        }
    }
}
