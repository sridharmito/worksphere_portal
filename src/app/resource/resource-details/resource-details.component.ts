import {TitleService} from '../../shared/services/title.service';
import {Component, ElementRef, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ResourceService} from '../services/resource.service';
import * as moment from 'moment';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

declare var iziToast: any;

@Component({
    selector: 'app-resource-details',
    templateUrl: './resource-details.component.html',
    styleUrls: ['./resource-details.component.css']
})
export class ResourceDetailsComponent implements OnInit {
    resourceId: string;
    resource: any;
    currentView: any;
    pt: any;
    startDate: any;
    endDate: any;
    selectedDate: any;
    reservations: any[] = [];
    events: any[] = [];
    modalRef: BsModalRef;
    @ViewChild('reserveModal') reserveModal: ElementRef;
    businessHours: any[];

    constructor(
        private route: ActivatedRoute,
        private resourceService: ResourceService,
        private router: Router,
        private titleService: TitleService,
        private modalService: BsModalService) {
        this.pt = {
            firstDayOfWeek: 0,
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            monthNames: [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro',
                        'Outubro', 'Novembro', 'Dezembro' ],
            monthNamesShort: [ 'Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez' ],
            today: 'Hoje',
            clear: 'Limpar'
        };

        this.startDate = moment().add(1, 'hour').startOf('hour').toDate();
        this.endDate = moment().add(2, 'hour').startOf('hour').toDate();
    }

    ngOnInit() {
        const mainMenu = JSON.parse(localStorage.getItem('menu'));
        this.titleService.setTitle({name: mainMenu.resources_caption, showSubTitle: false, url: '/home'});
        this.resourceId = this.route.snapshot.params['id'];
        this.loadResource(this.resourceId);
    }

    onDayClick(e) {
        const date = moment(e.date.format('YYYY-MM-DD HH:mm:ss'));
        const now = moment().utcOffset(e.date.utcOffset());
        if (date.isBefore(now)) {
            return;
        }

        this.selectedDate = new Date(e.date.format('YYYY-MM-DD HH:mm:ss'));
        this.startDate = new Date(e.date.format('YYYY-MM-DD HH:mm:ss'));
        this.endDate = new Date(e.date.add(1, 'hour').format('YYYY-MM-DD HH:mm:ss'));
        this.openModal();
    }

    openModal() {
        this.modalRef = this.modalService.show(this.reserveModal);
    }

    loadReservations(e) {
        this.currentView = e;
        const start  = e.view.activeRange.start.startOf('day').format('YYYY-MM-DD HH:mm:ss');
        const end  = e.view.activeRange.end.endOf('day').format('YYYY-MM-DD HH:mm:ss');
        this.resourceService.fetchReservations(this.resourceId, start, end).subscribe(
            reservations => {
                this.reservations = reservations._embedded.list;
                this.events = [];
                this.reservations.map(reservation => {
                    this.events.push({
                        id: reservation.id,
                        title: 'Reservado',
                        start: reservation.start_datetime,
                        end: reservation.finish_datetime
                    });
                });
            }
        )
    }

    doReserve() {

        if (moment(this.startDate).isAfter(moment(this.endDate))) {
            alert('O horário de fim deve ser maior do que o de início');
            return;
        }

        const payload = {
            start_datetime: moment(this.startDate).format('YYYY-MM-DD HH:mm:ss'),
            finish_datetime: moment(this.endDate).format('YYYY-MM-DD HH:mm:ss')
        };
        this.resourceService.createReservation(this.resource.id, payload).subscribe(
            success => {
                iziToast.success({
                    title: 'Sucesso',
                    message: 'Recurso reservado',
                    color: 'white',
                    icon: 'glyphicon glyphicon-ok',
                    iconColor: 'green',
                    layout: 2,
                    balloon: true,
                    position: 'bottomCenter',
                    pauseOnHover: false,
                });
                this.loadReservations(this.currentView);
                this.modalRef.hide();
            }, error => {
                let msg = 'Ocorreu um erro, tente novamente mais tarde';
                if (error.status === 409) {
                    msg = JSON.parse(error._body).detail;
                }

                iziToast.success({
                    title: 'Erro',
                    message: msg,
                    color: 'white',
                    icon: 'glyphicon glyphicon-remove',
                    iconColor: 'red',
                    layout: 2,
                    balloon: true,
                    position: 'bottomCenter',
                    pauseOnHover: false,
                });
            }
        )
    }

    private loadResource(id: any) {
        this.resourceService.getResourceById(id).subscribe(
            resource => {
                this.resource = resource;
                this.businessHours = [];
                for (const schedule of resource.schedule) {
                    this.businessHours.push({
                        dow: [schedule.weekday],
                        start: schedule.start_time,
                        end: schedule.finish_time
                    })
                }
            }
        );
    }
}
