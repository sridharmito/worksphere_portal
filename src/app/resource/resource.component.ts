import {TitleService} from '../shared/services/title.service';
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ResourceService} from './services/resource.service';
import * as moment from 'moment';

@Component({
    selector: 'app-resource',
    templateUrl: './resource.component.html',
    styleUrls: ['./resource.component.css']
})
export class ResourceComponent implements OnInit {
    resources: any[] = [];
    pt: any;

    constructor(
        private route: ActivatedRoute,
        private resourceService: ResourceService,
        private router: Router,
        private titleService: TitleService) {
        this.pt = {
            firstDayOfWeek: 0,
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
            dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
            monthNames: [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro',
                'Outubro', 'Novembro', 'Dezembro' ],
            monthNamesShort: [ 'Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez' ],
            today: 'Hoje',
            clear: 'Limpar'
        };
    }

    ngOnInit() {
        const mainMenu = JSON.parse(localStorage.getItem('menu'));
        this.titleService.setTitle({name: mainMenu.resources_caption, showSubTitle: false, url: '/home'});
        this.loadResources();
    }

    loadResources() {
        this.resourceService.getAvailableResource().subscribe(
            resources => {
                this.resources = resources._embedded.list;
            }
        )
    }

}
