import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ResourceComponent} from './resource.component';
import {ResourceDetailsComponent} from './resource-details/resource-details.component';
import {ResourceService} from './services/resource.service';
import {ResourceRoutingModule} from './resource.routing';
import {CalendarModule} from 'primeng/calendar';
import {ScheduleModule} from 'primeng/primeng';
import {FormsModule} from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap';
import {MyReservationsComponent} from './my-reservations/my-reservations.component';

@NgModule({
    imports: [
        CommonModule,
        CalendarModule,
        ScheduleModule,
        FormsModule,
        ModalModule,
        ResourceRoutingModule
    ],
    declarations: [
        ResourceComponent,
        ResourceDetailsComponent,
        MyReservationsComponent
    ],
    providers: [
        ResourceService
    ]
})
export class ResourceModule { }
