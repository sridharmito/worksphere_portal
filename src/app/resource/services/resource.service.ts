import {HeadersService} from '../../shared/services/headers.service';
import {Http, Response} from '@angular/http';
import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';

@Injectable()
export class ResourceService {

    constructor(private http: Http,
                private service: HeadersService) { }

    getAvailableResource() {
        return this.http.get(`${environment.url}/api/resources`, {headers: this.service.options()})
            .map((res: Response) => res.json());
    }

    getResourceById(id: number) {
        return this.http.get(`${environment.url}/api/resources/${id}`, {headers: this.service.options()})
            .map((res: Response) => res.json());
    }

    createReservation(id: any, reservation: any) {
        return this.http.post(`${environment.url}/api/resources/${id}/reservation`, reservation, {headers: this.service.options()})
            .map((res: Response) => res.json());
    }

    fetchReservations(id: any, start: string, finish: string) {
        return this.http.get(`${environment.url}/api/resources/${id}/reservation?start_date_min=${start}&start_date_max=${finish}`,
            {headers: this.service.options()})
            .map((res: Response) => res.json());
    }

    fetchMyReservations(minDate: string) {
        return this.http.get(`${environment.url}/api/users/me/resources/reservations?finish_date_min=${minDate}`,
            {headers: this.service.options()})
            .map((res: Response) => res.json());
    }

    deleteReservation(resourceId: number, id: number) {
        return this.http.delete(`${environment.url}/api/resources/${resourceId}/reservation/${id}`, {headers: this.service.options()})
            .map((res: Response) => res.json());
    }

}
