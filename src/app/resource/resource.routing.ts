import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ResourceComponent} from './resource.component';
import {ResourceDetailsComponent} from './resource-details/resource-details.component';
import {MyReservationsComponent} from './my-reservations/my-reservations.component';


const routes: Routes = [
    {
        path: '',
        component: ResourceComponent
    },
    {
        path: 'my-reservations',
        component: MyReservationsComponent
    },
    {
        path: 'reserve/:id',
        component: ResourceDetailsComponent
    },
    {
        path: 'detail/:id',
        component: ResourceDetailsComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ResourceRoutingModule { }
