import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { environment } from 'environments/environment';
import { HeadersService } from '../shared/services/headers.service';
import { HandleErrorService } from '../shared/services/handle-error.service';

@Injectable()
export class ContactListService {

    constructor(
        private http: Http,
        private handleError: HandleErrorService,
        private service: HeadersService) { }

    fetchAllContacts() {
            return this.http.get(`${environment.url}/api/user`, {headers: this.service.options()})
                .map((res: Response) => res.json())
                .catch(err => Observable.throw(this.handleError.errorToken()));
    }

}
