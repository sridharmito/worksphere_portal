import { Pipe, PipeTransform } from '@angular/core';
@Pipe({  name: 'orderBy' })
export class OrderrByPipe implements PipeTransform {

    transform(records: Array<any>): any {
        if (records)
        return records.sort(function(a, b){
            if(a.name < b.name){
                return -1 ;
            }else if(a.name > b.name){
                return 1 ;
            }else{
                return 0;
            }
        });
    };
}