import { Injectable } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import localeEnUs from '@angular/common/locales/en-US-POSIX';
import localeEs from '@angular/common/locales/es';

@Injectable()
export class SessionService {

    private _locale: string;

    setLocale(value: string) {
        localStorage.setItem('locale', value);
        this._locale = value;
        this.registerCulture(this._locale);
    }
    getLocale(): string {
        if (!this._locale && !localStorage.getItem('locale'))
            this._locale = 'es';
        else if (localStorage.getItem('locale'))
            this._locale = localStorage.getItem('locale');
        this.registerCulture(this._locale);
        return this._locale;

    }

    registerCulture(culture: string) {
        if (!culture) {
            return;
        }
        this._locale = culture;

        // Register locale data since only the en-US locale data comes with Angular
        switch (culture) {
            case 'es': {
                registerLocaleData(localeEs);
                break;
            }
            case 'en-US': {
                registerLocaleData(localeEnUs);
                break;
            }
            case 'pt-BR': {
                registerLocaleData(localePt);
                break;
            }
        }
    }
}