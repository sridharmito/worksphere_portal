import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { LoginService } from '../../login/services/login.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private loginService: LoginService) {}

    canActivate() {

        /**
         * Se existir o token, seguimos em frente.
         * Caso contrário, retorna para componente login.
         */
        if(this.loginService.getToken() !== null) {
            return true;
        } else {
            this.router.navigate(['/login'])
            return false;
        }
    }
}
