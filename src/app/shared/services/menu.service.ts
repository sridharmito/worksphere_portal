import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { environment } from '../../../environments/environment';
import { HeadersService } from './headers.service';
import { Observable } from 'rxjs/Observable';
import { HandleErrorService } from './handle-error.service';

@Injectable()
export class MenuService {

    constructor(
        private http: Http,
        private heraderService: HeadersService,
        private handleError: HandleErrorService) { }

    /**
     * Retorna o menu da aplicação
     */
    getMenu() {
        return this.http.get(`${environment.url}/api/user/me/menu`, { headers: this.heraderService.options() })
            .map((res: Response) => res.json())
            .catch(err => Observable.throw(this.handleError.error(err)));
    }


}
