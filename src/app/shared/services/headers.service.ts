import { Injectable } from '@angular/core';
import { Headers } from '@angular/http';

@Injectable()
export class HeadersService {

    token: string;

    constructor() {}

    /**
     * Adiciona o token no request
     */
    options() {
        this.token = localStorage.getItem('access_token');
        const headers: Headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append( 'Authorization', `Bearer ${this.token}` );
        //headers.append( 'Authorization', `Bearer 2f715ce6658e6045cb13e6fabb30c627639d1684` );
        return headers;
    }

}
