import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

declare var iziToast: any;

@Injectable()
export class HandleErrorService {

    constructor(private router: Router) { }

    /**
     * Returna um popup de erro do login
     */
    errorLogin(): Observable<any> {
        localStorage.clear();
        iziToast.destroy();
        let error = iziToast.error({
            title: 'Erro no login',
            message: 'Usuário ou senha inválidos',
            color: 'white',
            icon: 'glyphicon glyphicon-remove',
            iconColor: 'red',
            layout: 2,
            balloon: true,
            position: 'bottomCenter',
            pauseOnHover: false,
            onClose: () => {
                window.location.reload();
            }
        })

        return Observable.throw(error);

    }

    /**
     * Redireciona para um pagina de erro quando o token expira ou esta errado.
     */
    errorToken(): Observable<any> {
        iziToast.destroy();
        let error = iziToast.error({
            title: 'Erro',
            message: 'Ocorreu um erro, tente novamente mais tarde',
            color: 'white',
            icon: 'glyphicon glyphicon-remove',
            iconColor: 'red',
            layout: 2,
            balloon: true,
            position: 'bottomCenter',
            pauseOnHover: false,
        })

        return Observable.throw(error);
    }

    error(error: any): Observable<any> {
        if (error.status == 401) {
            localStorage.clear();
            this.router.navigate(['/login']);
        }
        return Observable.throw(error);
    }
}
