import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { environment } from '../../../environments/environment';
import { HeadersService } from './headers.service';

@Injectable()
export class GeneralServiceService {

    constructor(
        private http: Http,
        private heraderService: HeadersService) { }

    getMessageCategories() {
        return this.http.get(`${environment.url}/api/message-categories`, { headers: this.heraderService.options() })
            .map((res: Response) => res.json());
    }

    /**
     * Salva uma nova mensagem no backend.
     * @param {object} data Nova mensagem.
     */
    saveNewMessage(data: Object) {
        return this.http.post(`${environment.url}/api/message`, data, { headers: this.heraderService.options() })
            .map((res: Response) => res.json());
    }


    saveMessageImage(id: number, image: any) {
        return this.http.post(`${environment.url}/api/message/${id}/picture`, image, { headers: this.heraderService.options() })
            .map((res: Response) => res.json());
    }

    getTerms() {
        return this.http.get(`${environment.url}/api/terms`, {headers: this.heraderService.options()})
            .map((res: Response) => {
                const obj = res.json();
                obj.accept = true;
                return obj;
            });
    }

    getAbout() {
        return this.http.get(`${environment.url}/api/about`, {headers: this.heraderService.options()})
            .map((res: Response) => res.json());
    }

    acceptTerms() {
        return this.http.post(`${environment.url}/api/terms/accept`, {}, {headers: this.heraderService.options()})
            .map((res: Response) => res.json());
    }

    getCompanyConfig(companyId: string) {
        return this.http.get(`${environment.url}/api/company/${companyId}`, {headers: this.heraderService.options()})
            .map((res: Response) => res.json());
    }

}
