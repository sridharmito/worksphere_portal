import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class TitleService {

    newTitle = new Subject<any>();

    constructor() { }

    /**
     * Seta o nome da pagina
     * @param {object} title Nome da pagina
     */
    setTitle(title: Object): void {
        this.newTitle.next(title);
    }
}
