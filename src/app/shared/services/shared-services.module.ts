import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HandleErrorService } from './handle-error.service';
import { HeadersService } from './headers.service';
import { HomeNewsResolveService } from '../../home/services/news-resolve.service';
import { MenuService } from './menu.service';
import { MenuResolveService } from '../../home/services/menu-resolve.service';
import {CompanyService} from './company.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
      HandleErrorService,
      HeadersService,
      MenuService,
      MenuResolveService,
      HomeNewsResolveService,
      CompanyService
  ]
})
export class SharedServicesModule { }
