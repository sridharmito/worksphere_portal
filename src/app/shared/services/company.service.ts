import { Injectable } from '@angular/core';
import {Headers, Http, Response} from '@angular/http';

import { environment } from '../../../environments/environment';
import { HeadersService } from './headers.service';
import { Observable } from 'rxjs/Observable';
import { HandleErrorService } from './handle-error.service';

@Injectable()
export class CompanyService {

    constructor(
        private http: Http,
        private handleError: HandleErrorService) { }

    /**
     * Retorna as configurações de interface
     */
    getCompanyConfig(slug: string) {
        return this.http.get(`${environment.url}/api/company/${slug}`)
            .map((res: Response) => res.json())
            .catch(err => Observable.throw(this.handleError.error(err)));
    }

    /**
     * Retorna o portal da aplicação
     */
    getPortalConfig() {
        const token = localStorage.getItem('access_token');
        const headers: Headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append( 'Authorization', `Bearer ${token}` );

        return this.http.get(`${environment.url}/api/company/me/portal`, {headers: headers })
            .map((res: Response) => res.json())
            .catch(err => Observable.throw(this.handleError.error(err)));
    }
}
