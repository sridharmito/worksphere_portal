import { Directive, HostListener } from '@angular/core';

declare var $: any;

@Directive({
    selector: '[menuIn]'
})
export class MenuInDirective {

    constructor() { }

    @HostListener('click') onMouseEnter() {
        $(".navigation").toggleClass("navigation--open");
    }

    // @HostListener('mouseleave') onMouseLeave() {
    //     $(".navigation").removeClass("navigation--open");
    // }

}
