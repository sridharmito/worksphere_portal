import { Directive, ElementRef } from '@angular/core';

declare var $: any;

@Directive({
    selector: '[maskPhone]'
})
export class PhoneDirective {

    constructor(el: ElementRef) {
        $(el.nativeElement).mask('(00) 00000-0000');
    }

}
