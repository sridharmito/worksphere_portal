import { Directive, HostListener } from '@angular/core';

declare var $: any;

@Directive({
    selector: '[searchIn]'
})
export class SearchInDirective {

    constructor() { }

    @HostListener('mouseenter') onMouseEnter() {
        $(".menu").toggleClass("menu__search--open");
    }

    @HostListener('mouseleave') onMouseLeave() {
        $(".menu").toggleClass("menu__search--open");
    }

}
