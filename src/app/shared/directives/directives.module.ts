import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PhoneDirective } from './phone.directive';
import { MenuInDirective } from './menu-in.directive';
import { SearchInDirective } from './search-in.directive';
import { NotificationInDirective } from './notification-in.directive';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        PhoneDirective,
        MenuInDirective,
        SearchInDirective,
        NotificationInDirective

    ],
    exports: [
        PhoneDirective,
        MenuInDirective,
        SearchInDirective,
        NotificationInDirective
    ]
})
export class DirectivesModule { }
