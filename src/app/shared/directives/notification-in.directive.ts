import { Directive, HostListener } from '@angular/core';

declare var $: any;

@Directive({
    selector: '[notificationIn]'
})
export class NotificationInDirective {

    constructor() { }

    @HostListener('mouseenter') onMouseEnter() {
        $(".notification").toggleClass("notification--open");
    }

    @HostListener('mouseleave') onMouseLeave() {
        $(".notification").toggleClass("notification--open");
    }

}
