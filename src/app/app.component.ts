import { Component, Inject, OnInit } from '@angular/core';
import { Event, NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import { CompanyService } from './shared/services/company.service';
import { environment } from '../environments/environment';
import { DOCUMENT } from '@angular/platform-browser';
import { LoginService } from './login/services/login.service';

declare var $: any;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    loading = true;
    user = null;
    constructor(private router: Router, private companyService: CompanyService, private loginService: LoginService, @Inject(DOCUMENT) private document) {
        router.events.subscribe((routerEvent: Event) => this.checkRouterEvent(routerEvent));



        router.events
            .filter(event => event instanceof NavigationEnd)
            .subscribe((event: NavigationEnd) => {
                window.scroll(0, 0);
            });


        router.events
            .filter(event => event instanceof NavigationStart)
            .subscribe((event: NavigationStart) => {
                if (event.url === '/accept-terms') {
                    return;
                }

                const user = JSON.parse(localStorage.getItem('user'));
                if (user && user.should_accept_terms) {
                    this.router.navigate(['/accept-terms']);
                }
            });
    }

    ngOnInit(): void {
        const hostname = window.location.hostname;
        let slug: string;
        if (hostname === 'localhost' || hostname === 'portal.worksphere.com.br' || hostname === 'portal.b2app.dev.lab3dvlp.com') {
            slug = 'b2mdia';
        } else {
            slug = hostname.split('.')[0];
        }

        this.companyService.getCompanyConfig(slug)
            .subscribe(
                (res) => {
                    environment.company_id = res.id;
                    environment.saml_enabled = res.saml_enabled;
                    this.document.getElementById('theme').href = environment.url + '/company/portalcss/' + res.id;
                }
            );
    }

    checkRouterEvent(routerEvent: Event): void {
        if (routerEvent instanceof NavigationStart) {
            this.loading = true;
        }

        if (routerEvent instanceof NavigationEnd ||
            routerEvent instanceof NavigationCancel ||
            routerEvent instanceof NavigationError) {
            this.loading = false;
        }
    }



}
