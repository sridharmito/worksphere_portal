import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { LoginComponent } from './login.component';
import { LoginService } from './services/login.service';
import { LoginRoutingModule } from './login.routing';
import {LoginSamlComponent} from './login-saml.component';

@NgModule({
    imports: [
        CommonModule,
        LoginRoutingModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        LoginComponent,
        LoginSamlComponent
    ],
    providers: [
        LoginService
    ]
})
export class LoginModule { }
