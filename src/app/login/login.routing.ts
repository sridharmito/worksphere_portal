import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login.component';
import {LoginSamlComponent} from './login-saml.component';


const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'login/local',
        component: LoginComponent
    },
    {
        path: 'login/saml',
        component: LoginSamlComponent
    },
    {
        path: 'login/saml/:companyId',
        component: LoginSamlComponent
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LoginRoutingModule { }
