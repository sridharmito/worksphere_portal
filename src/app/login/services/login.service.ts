import { Injectable } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { Router } from '@angular/router';
import { HeadersService } from '../../shared/services/headers.service';

import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { HandleErrorService } from '../../shared/services/handle-error.service';

declare var iziToast: any;

@Injectable()
export class LoginService {

    constructor(
        private http: Http,
        private service: HeadersService,
        private handleError: HandleErrorService) { }

    /**
     * Raliza o login do usuário.
     * @param {object} login Objeto com email e password para autenticação
     */
    login(login: Object): Observable<any> {
        const payload = {
            'grant_type': environment.grant_type,
            'username': environment.company_id + '|' + login['email'],
            'password': login['password'],
            'client_id': environment.client_id,
            'client_secret': environment.client_secret
        }

        // Retorna um objeto com a resposta do login
        return this.http.post(`${environment.url}/oauth`, payload)
            .map((res: Response) => res.json())
            .catch(this.handleError.errorLogin);
    }

    loginSaml(attrs: string, company: string): Observable<any> {
        // Retorna um objeto com a resposta do login
        return this.http.get(`${environment.url}/saml/${company}?attrs=${attrs}&company=${company}`)
            .map((res: Response) => res.json())
            .catch(this.handleError.errorLogin);
    }

    /**
     * Retorna o token
     */
    getToken() {
        return localStorage.getItem('access_token');
    }

    /**
     * Retorna as informações do usuário atual
     */
    getUserInfo() {
        return this.http.get(`${environment.url}/api/user/me?origin=W`, {headers: this.service.options()})
            .map((res: Response) => res.json())
            .catch(this.handleError.errorToken);
    }
}
