import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from './services/login.service';
import {ActivatedRoute, Router} from '@angular/router';
import {environment } from '../../environments/environment';
import {CompanyService} from "../shared/services/company.service";

declare var iziToast: any;
declare var $: any;
declare var swal: any;

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    form: FormGroup;
    perfil: Object;
    headerLogoUrl: string;
    footerLogoUrl: string;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private service: LoginService,
        private companyService: CompanyService,
        private route: ActivatedRoute) { }

    ngOnInit() {
        
        this.form = this.fb.group({
            email: ['', [Validators.required]],
            password: ['', Validators.required ]
        });

        if (this.route.snapshot.url.length === 1 && environment.saml_enabled) {
            this.router.navigate(['/login/saml']);
        }

        const hostname = window.location.hostname;
        let slug: string;
        if (hostname === 'localhost' || hostname === 'portal.worksphere.com.br') {
            slug = 'b2mdia';
        } else {
            slug = hostname.split('.')[0];
        }

        this.companyService.getCompanyConfig(slug)
            .subscribe(
                (res) => {
                    this.headerLogoUrl = res.portal.url_logo_header;
                    this.footerLogoUrl = res.portal.url_logo_footer;
                    if (this.route.snapshot.url.length === 1 && res.saml_enabled) {
                        this.router.navigate(['/login/saml']);
                    }
                }
            );
    }

    /**
     * Realiza o login do usuário.
     * @param {object} user Objeto contendo os dados para login
     */

    login(user: any) {
        this.service.login(user)
            .subscribe(
                token => {
                    localStorage.setItem('access_token', token['access_token']);
                    localStorage.setItem('refresh_token', token['refresh_token']);
                    if (this.service.getToken()) {
                        this.getUserInfo();
                        this.router.navigate(['/']);
                    }
                }
            );
    }

    /**
     * Buscas os dados do usuário e salva no localStorage
     */
    private getUserInfo() {
        return this.service.getUserInfo()
            .subscribe(
                user => localStorage.setItem('user', JSON.stringify(user)),
                err => console.log(err)
            );
    }

    showResetPass() {

        swal({
          title: 'Alterar senha',
          text: 'Você tem certeza que dejeja alterar sua senha?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Sim, Alterar!',
          cancelButtonText: 'Alterar depois',
          showLoaderOnConfirm: true,
          preConfirm: () => {
              return new Promise((resolve, reject) => {
                  setTimeout(() => {
                    resolve();
                  }, 2000);
              });
          }
        })
        .then(() => {
                  swal(
                    'Sucesso!',
                    `Um email foi enviado para EMAIL, com as instruções de como alterar sua senha.`,
                    'success'
                  );
        })
        .catch(() => {
            swal(
              'Cancelado',
              'Alteração de senha cancelada com sucesso!',
              'error'
            );
        });
    }
}
