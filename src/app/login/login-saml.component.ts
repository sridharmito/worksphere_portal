import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {LoginService} from './services/login.service';
import {CompanyService} from "../shared/services/company.service";

@Component({
    selector: 'app-login-saml',
    templateUrl: './login-saml.component.html',
    styleUrls: ['./login-saml.component.css']
})
export class LoginSamlComponent implements OnInit {
    headerLogoUrl: string;
    footerLogoUrl: string;
    samlUrl: string;

    constructor(private route: ActivatedRoute, private loginService: LoginService, private companyService: CompanyService,
                private router: Router) {
    }

    ngOnInit(): void {

        const hostname = window.location.hostname;
        let slug: string;
        if (hostname === 'localhost' || hostname === 'portal.worksphere.com.br') {
            slug = 'b2mdia';
        } else {
            slug = hostname.split('.')[0];
        }

        this.companyService.getCompanyConfig(slug)
            .subscribe(
                (res) => {
                    this.headerLogoUrl = res.portal.url_logo_header;
                    this.footerLogoUrl = res.portal.url_logo_footer;
                    this.samlUrl = 'https://sso.worksphere.com.br/saml/worksphere-web.php?company_id=' + res.id;
                }
            );

        this.route.queryParams.subscribe(params => {
            if (params['attrs'] && params['company']) {
                this.loginService.loginSaml(params['attrs'], params['company'])
                    .subscribe(token => {
                        localStorage.setItem('access_token', token['access_token']);
                        localStorage.setItem('refresh_token', token['refresh_token']);
                        if (this.loginService.getToken()) {
                            this.getUserInfo();
                            this.router.navigate(['/']);
                        }
                    });
            }
        });
    }

    /**
     * Buscas os dados do usuário e salva no localStorage
     */
    private getUserInfo() {
        return this.loginService.getUserInfo()
            .subscribe(
                user => localStorage.setItem('user', JSON.stringify(user)),
                err => console.log(err)
            );
    }

}
