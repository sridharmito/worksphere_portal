import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule, LOCALE_ID,OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, BaseRequestOptions, RequestOptions } from '@angular/http';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import {CarouselModule, ModalModule} from 'ngx-bootstrap';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import 'rxjs/add/operator/catch'
import 'rxjs/add/operator/map'
import 'rxjs/add/observable/throw'
import { HashLocationStrategy, LocationStrategy } from'@angular/common';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthGuard } from './shared/guards/auth.guard';
import { CarouselComponent } from './home/carousel/carousel.component';
import { HomeComponent } from './home/home.component';
import { LayoutModule } from './layout/layout.module';
import { LoginModule } from './login/login.module';
import { PerfilModule } from './perfil/perfil.module';
import { SharedServicesModule } from './shared/services/shared-services.module';
import { NewsModule } from './news/news.module';
import { TermsComponent } from './home/terms/terms.component';
import {AboutComponent} from './home/about/about.component';
import {ResourceModule} from './resource/resource.module';
import {ContactusModule} from './contactus/contactus.module';
import {PageModule} from "./page/page.module";
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import {SessionService} from './utils/session.service';



@NgModule({
    declarations: [
        AppComponent,
        CarouselComponent,
        HomeComponent,
        TermsComponent,
        AboutComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        CarouselModule.forRoot(),
        TabsModule.forRoot(),
        BsDropdownModule.forRoot(),
        ModalModule.forRoot(),
        InfiniteScrollModule,
        FormsModule,
        LayoutModule,
        LoginModule,
        PerfilModule,
        NewsModule,
        PageModule,
        ResourceModule,
        ContactusModule,
        ReactiveFormsModule,
        HttpModule,
        AppRoutingModule,
        SharedServicesModule,
        ScrollToModule.forRoot()        
        
    ],
    providers: [
        SessionService,
        AuthGuard,
       {
        provide: LOCALE_ID,
        deps: [SessionService],
        useFactory: (sessionService) => sessionService.getLocale()
      },
        //   {
        //      provide: LOCALE_ID,
        //       useValue: "pt-BR"
        //  },
        { provide: LocationStrategy, useClass: HashLocationStrategy }
    ],
    bootstrap: [
        AppComponent
    ]
})


export class AppModule {
    
}
