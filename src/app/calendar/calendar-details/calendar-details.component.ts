import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TitleService } from '../../shared/services/title.service';

import { environment } from '../../../environments/environment';

declare var moment: any;

@Component({
    selector: 'app-calendar-details',
    templateUrl: './calendar-details.component.html'
})
export class CalendarDetailsComponent implements OnInit {

    event: any;
    isSameDay: boolean;
    accessToken: string;

    constructor(private route: ActivatedRoute, private titleService: TitleService) { }

    ngOnInit() {
        this.route.data.subscribe(event => this.event = event['data']);
        this.isSameDay = moment(this.event['start_date']).isSame(this.event['end_date'], 'day');
        this.titleService.setTitle({ name: 'Calendário', subTitle: 'Evento', showSubTitle: true, url: '/calendar' });
        this.accessToken = localStorage.getItem('access_token');
    }

    imageUrl(id: string) {
        let url = `${environment.url}/file/content/${id}`;
        return `url(${url})`;
    }

}
