import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { CalendarService } from './calendar.service';

@Injectable()
export class CalendarResolve implements Resolve<any> {

    constructor(private calendarService: CalendarService) { }

    resolve(route: ActivatedRouteSnapshot) {
        let id = route.params['id'];
        return this.calendarService.getEvent(id);
    }
}
