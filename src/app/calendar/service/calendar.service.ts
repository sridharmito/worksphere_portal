import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { environment } from '../../../environments/environment';
import { HeadersService } from '../../shared/services/headers.service';

@Injectable()
export class CalendarService {

    constructor(private http: Http,
        private service: HeadersService) { }

    /**
     * Retorna o calendário de eventos, com base no ID do usuário enviado.
     * @param {number} id Id do usuário
     */
    getEvent(id: number) {
        return this.http.get(`${environment.url}/api/event`, {headers: this.service.options()})
            .map((res: Response) => res.json());
    }

    /**
     * Retorna o envento com base no ID enviando.
     * @param {number} id ID do envento solicitado
     */
    getEventById(id: number) {
        return this.http.get(`${environment.url}/api/event/${id}`, {headers: this.service.options()})
            .map((res: Response) => res.json());
    }

    /**
     * Retorna os proximos eventos
     * @param {string} start Data inicial
     * @param {string} end Data final
     */
    getNextEvents(start: string, end: string) {
        return this.http.get(`${environment.url}/api/event?start_date=${start}&finish_date=${end}`, {headers: this.service.options()})
            .map((res: Response) => res.json());
    }

}
