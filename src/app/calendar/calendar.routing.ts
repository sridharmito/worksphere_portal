import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CalendarComponent } from './calendar.component';
import { CalendarResolve } from './service/calendar-resolve.service';
import { CalendarDetailsComponent } from './calendar-details/calendar-details.component';
import { CalendarDetailsResolve } from './service/calendar-details-resolve.service';


const routes: Routes = [
    {
        path: '',
        component: CalendarComponent,
        resolve: { data: CalendarResolve }
    },
    {
        path: ':id',
        component: CalendarDetailsComponent,
        resolve: { data: CalendarDetailsResolve }
    }

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CalendarRoutingModule { }
