import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScheduleModule } from 'primeng/primeng';

import { CalendarComponent } from './calendar.component';
import { CalendarService } from './service/calendar.service';
import { CalendarResolve } from './service/calendar-resolve.service';
import { CalendarDetailsComponent } from './calendar-details/calendar-details.component';
import { CalendarDetailsResolve } from './service/calendar-details-resolve.service';
import { CalendarRoutingModule } from './calendar.routing';

@NgModule({
    imports: [
        CommonModule,
        CalendarRoutingModule,
        ScheduleModule
    ],
    declarations: [
        CalendarComponent,
        CalendarDetailsComponent
    ],
    providers: [
        CalendarService,
        CalendarResolve,
        CalendarDetailsResolve
    ]
})
export class CalendarModule { }
