import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TitleService } from '../shared/services/title.service';
import { CalendarService } from './service/calendar.service';

declare var $: any;
declare var moment: any;

@Component({
    selector: 'app-calendar',
    templateUrl: './calendar.component.html',
    styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit{

    allEvents: any;
    events: Array<Object>;
    nextEvents: Array<Object>;
    todayEvents: Array<Object>;
    oldEvents: Array<Object>;
    headerConfig: Object;

    constructor(
        private route: ActivatedRoute,
        private calendarService: CalendarService,
        private router: Router,
        private titleService: TitleService) { }

    ngOnInit() {
        const mainMenu = JSON.parse(localStorage.getItem('menu'));
        this.titleService.setTitle({ name: mainMenu.events_caption, showSubTitle: false, url: '/home' });

        this.headerConfig = {
            left: 'prev',
            center: 'title',
            right: 'next'
        };

        this.route.data.subscribe(events => {
            this.parseEvents(events.data._embedded.list);
        });

    }


    handleEventClick(e) {
        this.router.navigate(['/calendar', e.calEvent.id])
    }

    loadEvents(e) {
        const start  = e.view.activeRange.start.format('YYYY-MM-DD');
        const end  = e.view.activeRange.end.format('YYYY-MM-DD');

        this.calendarService.getNextEvents(start, end)
            .subscribe(events => {
                this.parseEvents(events._embedded.list);
            })
    }

    parseEvents(events) {

        this.nextEvents = [];
        this.todayEvents = [];
        this.oldEvents = [];
        this.events = [];
        events.map(event => {
            this.events.push({
                id: event.id,
                title: event.name,
                start: event.start_date,
                end: event.end_date
            });

            const start = moment(event.start_date);
            const now = moment();
            if (start.format('YYYY-MM-DD') === now.format('YYYY-MM-DD')) {
                this.todayEvents.push(event);
            } else if (start.isBefore(now)) {
                this.oldEvents.push(event);
            } else {
                this.nextEvents.push(event);
            }
        });
    }
}
