import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import {DropdownModule, FileUploadModule, GrowlModule} from 'primeng/primeng';
import {ContactusComponent} from './contactus.component';
import {ContactusRoutingModule} from './contactus.routing';
import {MessageListComponent} from './message-list/message-list.component';
import {MessageDetailsComponent} from './message-details/message-details.component';
import {MessageReplyComponent} from './message-reply/message-reply.component';
import {MessageService} from './services/message.service';

@NgModule({
    imports: [
        CommonModule,
        ContactusRoutingModule,
        FormsModule,
        FileUploadModule,
        ReactiveFormsModule,
        FileUploadModule,
        DropdownModule,
        GrowlModule
    ],
    declarations: [
        ContactusComponent,
        MessageListComponent,
        MessageDetailsComponent,
        MessageReplyComponent
    ],
    providers: [
        MessageService
    ]
})
export class ContactusModule { }
