import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as moment from 'moment';
import {MessageService} from '../services/message.service';
import {TitleService} from '../../shared/services/title.service';

declare var iziToast: any;

@Component({
    selector: 'app-message-details',
    templateUrl: './message-details.component.html',
    styleUrls: ['./message-details.component.css']
})
export class MessageDetailsComponent implements OnInit {
    
    messageId: string;
    message: any = {};
    replies: any[] = [];

    show = false;

    constructor(
        private route: ActivatedRoute,
        private MessageService: MessageService,
        private router: Router,
        private titleService: TitleService) {

    }

    ngOnInit() {
        this.titleService.setTitle({name: 'Detalhes da mensagem', showSubTitle: false, url: '/home'});
        this.messageId = this.route.snapshot.params['id'];
        this.loadMessage(this.messageId);
        this.loadReplies(this.messageId);
        this.changeRepliesStatus(this.messageId);
    }

    loadMessage(id: string) {
        this.MessageService.getMessage(id).subscribe(
            message => {
                this.message = message;
            }
        )
    }

    loadReplies(id: string) {
        this.MessageService.fetchMessageReplies(id).subscribe(
            messages => {
                this.replies = messages._embedded.list;
            }
        )
    }

    changeRepliesStatus(id: string) {
        this.MessageService.messageRepliesStatus(id).subscribe();
    }

}
