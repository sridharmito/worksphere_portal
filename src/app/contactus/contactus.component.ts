import {Component, OnInit, ViewChild} from '@angular/core';

import { LoginService } from '../login/services/login.service';
import { GeneralServiceService } from '../shared/services/general-service.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../environments/environment';
import { FileUpload } from 'primeng/primeng';
import {ActivatedRoute, Router} from "@angular/router";
import {TitleService} from "../shared/services/title.service";

declare var $: any;
declare var iziToast: any;

@Component({
    selector: 'app-contactus',
    templateUrl: './contactus.component.html',
    styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit {
    @ViewChild('upload') upload: FileUpload;

    categories: Array<Object>;
    createdMessageId: number;
    extraEnabled = false;
    extraCaption = '';
    form: FormGroup;
    msgs: Array<any> = [];
    userInfo: any;
    isSuggestion = false;
    sending = false;
    urlUpload: string = environment.url
    contactList:any ;

    constructor(
        private loginService: LoginService,
        private categoriesService: GeneralServiceService,
        private titleService: TitleService,
        private router: Router,
        private route: ActivatedRoute,
        private fb: FormBuilder) { }

    ngOnInit() {
        
        if(this.route.snapshot.url[0] && this.route.snapshot.url[0].path)
            this.isSuggestion = this.route.snapshot.url[0].path === 'suggestion';

        this.form = this.fb.group({
            subject: ['', Validators.required],
            message: ['', Validators.required],
            extra: [''],
            category_id: ''
        });

        this.loginService.getUserInfo()
            .subscribe(user => {
                this.userInfo = user;
                this.titleService.setTitle({
                    name: this.isSuggestion ? this.userInfo.suggestion_caption : this.userInfo.message_caption,
                    subTitle: false,
                    url: '/home'
                });
            });

        this.categoriesService.getMessageCategories()
            .subscribe(catgories => this.categories = catgories['_embedded'].list);

        const user = JSON.parse(localStorage.getItem('user'));
        this.extraEnabled = user.message_extra_enabled;
        this.extraCaption = user.message_extra_caption;
    }

    onBeforeSend(e) {
        const token = localStorage.getItem('access_token');
        e.xhr.open('POST', this.urlUpload, true);
        e.xhr.setRequestHeader('Authorization', `Bearer ${token}`);
    }

    onUpload(e) {
        iziToast.success({
            title: 'Sucesso',
            message: 'Mensagem enviada',
            color: 'white',
            icon: 'glyphicon glyphicon-ok',
            iconColor: 'green',
            layout: 2,
            balloon: true,
            position: 'bottomCenter',
            pauseOnHover: false,
        });
        this.router.navigate(['/']);
    }
        
    saveMessage(newMessage) {
        this.sending = true;
        this.categoriesService.saveNewMessage(newMessage)
            .subscribe(result => {
                this.createdMessageId = result.id;
                if (this.upload.files.length > 0) {
                    this.urlUpload = environment.url + '/api/message/' + this.createdMessageId + '/picture';
                    this.upload.upload();
                } else {
                    iziToast.success({
                        title: 'Sucesso',
                        message: 'Mensagem enviada',
                        color: 'white',
                        icon: 'glyphicon glyphicon-ok',
                        iconColor: 'green',
                        layout: 2,
                        balloon: true,
                        position: 'bottomCenter',
                        pauseOnHover: false,
                    });
                    this.sending = false;
                    this.router.navigate(['/']);
                }
            });
    }

}
