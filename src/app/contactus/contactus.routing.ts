import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ContactusComponent} from './contactus.component';
import {MessageListComponent} from './message-list/message-list.component';
import {MessageDetailsComponent} from './message-details/message-details.component';
import {MessageReplyComponent} from './message-reply/message-reply.component';


const routes: Routes = [
    {
        path: '',
        component: ContactusComponent
    },
    {
        path: 'suggestion',
        component: ContactusComponent
    },
    {
        path: 'my-messages',
        component: MessageListComponent
    },
    {
        path: 'details/:id',
        component: MessageDetailsComponent
    },
    {
        path: 'message-reply/:id',
        component: MessageReplyComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ContactusRoutingModule { }
