import {HeadersService} from '../../shared/services/headers.service';
import {Http, Response} from '@angular/http';
import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';

@Injectable()
export class MessageService {

    constructor(private http: Http,
                private service: HeadersService) { }

    getMessages() {
        return this.http.get(`${environment.url}/api/message`, {headers: this.service.options()})
            .map((res: Response) => res.json());
    }

    getMessage(msgId: string) {
        return this.http.get(`${environment.url}/api/message/${msgId}`, {headers: this.service.options()})
            .map((res: Response) => res.json());
    }

    fetchMessageReplies(msgId: string) {
        return this.http.get(`${environment.url}/api/message-replies?message_id=${msgId}`,
            {headers: this.service.options()})
            .map((res: Response) => res.json());
    }

    saveNewMessageReply(reply: any) {
        return this.http.post(`${environment.url}/api/message-reply`, reply, {headers: this.service.options()})
            .map((res: Response) => res.json());
    }

    messageRepliesStatus(id: any) {
        return this.http.patch(`${environment.url}/api/message-replies/${id}`, {}, {headers: this.service.options()})
            .map((res: Response) => res.json());
    }
    
}
