import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as moment from 'moment';
import {MessageService} from '../services/message.service';
import {TitleService} from '../../shared/services/title.service';

declare var iziToast: any;

@Component({
    selector: 'app-message-list',
    templateUrl: './message-list.component.html',
    styleUrls: ['./message-list.component.css']
})
export class MessageListComponent implements OnInit {
    
    messages: any[] = [];

    show = false;

    constructor(
        private route: ActivatedRoute,
        private MessageService: MessageService,
        private router: Router,
        private titleService: TitleService) {

    }

    ngOnInit() {
        this.titleService.setTitle({name: 'Minhas Mensagens', showSubTitle: false, url: '/home'});
        this.loadMessages();
    }

    loadMessages() {
        this.MessageService.getMessages().subscribe(
            messages => {
                this.messages = messages._embedded.list;
            }
        )
    }

    openMessage(message) {
        this.router.navigate(['/contactus/details', message.id]);
        this.show = false;
    }
}
