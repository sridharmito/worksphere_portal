import {Component, OnInit} from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { ActivatedRoute, Router } from "@angular/router";
import { MessageService } from '../services/message.service';
import { TitleService } from "../../shared/services/title.service";

declare var $: any;
declare var iziToast: any;

@Component({
    selector: 'app-message-reply',
    templateUrl: './message-reply.component.html',
    styleUrls: ['./message-reply.component.css']
})
export class MessageReplyComponent implements OnInit {
    
    messageId: string;
    form: FormGroup;
    sending = false;
    msgs = [];
    constructor(
        private titleService: TitleService,
        private router: Router,
        private route: ActivatedRoute,
        private MessageService: MessageService,
        private fb: FormBuilder) { }

    ngOnInit() {
        
        this.messageId = this.route.snapshot.params['id'];

        this.form = this.fb.group({
            message: ['', Validators.required],
        });

    }

    saveMessage(newMessage) {
        this.sending = true;
        newMessage.message_id = this.messageId;
        this.MessageService.saveNewMessageReply(newMessage)
            .subscribe(result => {
                    iziToast.success({
                        title: 'Sucesso',
                        message: 'Mensagem enviada',
                        color: 'white',
                        icon: 'glyphicon glyphicon-ok',
                        iconColor: 'green',
                        layout: 2,
                        balloon: true,
                        position: 'bottomCenter',
                        pauseOnHover: false,
                    });
                    this.sending = false;
                    this.router.navigate(['contactus/details', this.messageId]);
            });
    }

}
