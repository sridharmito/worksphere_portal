import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfilChangePassComponent } from './perfil-change-pass.component';

describe('PerfilChangePassComponent', () => {
  let component: PerfilChangePassComponent;
  let fixture: ComponentFixture<PerfilChangePassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerfilChangePassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerfilChangePassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
