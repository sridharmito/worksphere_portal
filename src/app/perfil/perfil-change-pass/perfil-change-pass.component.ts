import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { PerfilService } from '../service/perfil.service';

declare var iziToast: any;

@Component({
    selector: 'app-perfil-change-pass',
    templateUrl: './perfil-change-pass.component.html',
    styleUrls: ['./perfil-change-pass.component.css']
})
export class PerfilChangePassComponent implements OnInit {

    spinner: boolean;
    resetPass: FormGroup;

    constructor(
        private fb: FormBuilder,
        private location: Location,
        private perfilService: PerfilService) { }

    ngOnInit() {
        this.resetPass = this.fb.group({
            'password': ['', Validators.required],
            'password1': ['', [Validators.required]],
        },{ validator: this.comparePass});
    }


    comparePass(c: AbstractControl) {

        let p1 = c.get('password');
        let p2 = c.get('password1');

        if(p1.pristine || p2.pristine) return null;
        if(p1.value === p2.value) return null;

        return { 'match': true }
    }

    back() {
        this.location.back();
    }

    newPassword(newPass) {
        this.spinner = true;
        let pass = { password: newPass.password };
        this.perfilService.changePassword(pass)
            .subscribe(newpass => {
                this.spinner = false;
                iziToast.success({
                    title: 'Sucesso',
                    message: 'Sua senha foi alterada com sucesso!',
                    icon: 'fa fa-info',
                    iconColor: 'green',
                    layout: 2,
                    balloon: true,
                    color: 'white',
                    position: 'bottomCenter',
                    pauseOnHover: false,
                });

                this.resetPass.reset();
            })

    }
}
