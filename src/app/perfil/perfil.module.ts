import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import {DropdownModule, FileUploadModule, GrowlModule} from 'primeng/primeng';

import { PerfilComponent } from './perfil.component';
import { PerfilRoutingModule } from './perfil.routing';
import { PerfilService } from './service/perfil.service';
import { PerfilResolve } from './service/perfil-resolve.service';
import { PerfilEditComponent } from './perfil-edit/perfil-edit.component';
import { PerfilCurrentComponent } from './perfil-current/perfil-current.component';
import { DirectivesModule } from '../shared/directives/directives.module';
import { PerfilChangePassComponent } from './perfil-change-pass/perfil-change-pass.component';
import { ModalModule } from 'ngx-bootstrap/modal';

@NgModule({
    imports: [
        CommonModule,
        PerfilRoutingModule,
        ReactiveFormsModule,
        DirectivesModule,
        FileUploadModule,
        DropdownModule,
        ModalModule,
        GrowlModule
    ],
    declarations: [
        PerfilComponent,
        PerfilEditComponent,
        PerfilCurrentComponent,
        PerfilChangePassComponent
    ],
    providers: [
        PerfilService,
        PerfilResolve
    ]
})
export class PerfilModule { }
