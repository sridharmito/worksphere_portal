import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {  FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { environment } from '../../environments/environment';
import { TitleService } from '../shared/services/title.service';

declare var swal: any;

@Component({
    selector: 'app-perfil',
    templateUrl: './perfil.component.html',
    styleUrls: ['./perfil.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class PerfilComponent implements OnInit {

    perfil: any;
    showPerfil: boolean;
    urlUpload: string = environment.url + '/api/user/me/picture';
    urlUploadBg: string = environment.url + '/api/user/me/background';
    uploadedFiles: Array<any> = [];
    msgs: Array<any> = [];

    constructor(private route: ActivatedRoute, private titleService: TitleService, private router: Router) { }

    ngOnInit() {
        this.route.data.subscribe(data => this.perfil = data['data']);
        this.titleService.setTitle({ name: 'Perfil', subTitle: false, url: '/home' });
    }

    editPerfil() {
        this.showPerfil = !this.showPerfil;
    }

    onBeforeSend(e) {
        let token = localStorage.getItem('access_token');
        e.xhr.open('POST', this.urlUpload, true);
        e.xhr.setRequestHeader('Authorization', `Bearer ${token}`);
    }

    onBeforeSendBg(e) {
        let token = localStorage.getItem('access_token');
        e.xhr.open('POST', this.urlUploadBg, true);
        e.xhr.setRequestHeader('Authorization', `Bearer ${token}`);
    }

    logout() {
        localStorage.clear();
        this.router.navigate(['/login']);
    }

    onUpload(e) {
        window.location.reload();

    }

    onUploadBg(e) {
        window.location.reload();

    }

    onError(e) {
    }
}
