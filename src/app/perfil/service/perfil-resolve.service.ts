import { Injectable } from '@angular/core';
import { Resolve,  ActivatedRouteSnapshot } from '@angular/router';
import { PerfilService } from './perfil.service';

@Injectable()
export class PerfilResolve implements Resolve<any> {

    constructor(private perfilService: PerfilService) { }

    resolve(route: ActivatedRouteSnapshot) {
        let id = route.params['id'];
        return this.perfilService.getUserPerfil(id);
    }

}
