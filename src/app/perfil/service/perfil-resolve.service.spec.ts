import { TestBed, inject } from '@angular/core/testing';

import { PerfilResolveService } from './perfil-resolve.service';

describe('PerfilResolveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PerfilResolveService]
    });
  });

  it('should ...', inject([PerfilResolveService], (service: PerfilResolveService) => {
    expect(service).toBeTruthy();
  }));
});
