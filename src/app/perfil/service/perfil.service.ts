import { Injectable } from '@angular/core';

import { environment } from '../../../environments/environment';
import { Http, Response } from '@angular/http';
import { HeadersService } from '../../shared/services/headers.service';

@Injectable()
export class PerfilService {

    constructor(private http: Http,
        private service: HeadersService) {}

    /**
     * Altera asenha do usuário
     * @param {string} newPass A nova senha do usuário
     */
    changePassword(newPass: Object) {
        return this.http.patch(`${environment.url}/api/user/me/password`, newPass, {headers: this.service.options()})
            .map((res: Response) => res.json());
    }

    /**
     * Retorna o perfil do usuário com base no ID enviado.
     * @param {number} id ID do usuário que deseja buscar o perfile
     */
    getUserPerfil(id: number) {
        return this.http.get(`${environment.url}/api/user/${id}`, {headers: this.service.options()})
            .map((res: Response) => res.json());
    }

    /**
     * Atualiza o perfil do usuário
     * @param {object} perfil Object que contem as informações a serem atualizadas no perfil do usuário
     */
    updatePerfil(perfil: Object) {
        return this.http.patch(`${environment.url}/api/user/me`, perfil, {headers: this.service.options()})
            .map((res: Response) => res.json());
    }

}
