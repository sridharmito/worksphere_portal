import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-perfil-current',
    templateUrl: './perfil-current.component.html',
    styleUrls: ['./perfil-current.component.css']
})
export class PerfilCurrentComponent implements OnInit {

    perfil: any;
    showPerfil: boolean;

    constructor(private route: ActivatedRoute) { }

    ngOnInit() {

        let currentPerfil = JSON.parse(localStorage.getItem('user'));
        if (currentPerfil) return this.perfil = currentPerfil;

        this.route.data.subscribe(data => {
            this.perfil = data['data'];
            localStorage.setItem('user', JSON.stringify(data['data']));
        });
    }

}
