import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfilCurrentComponent } from './perfil-current.component';

describe('PerfilCurrentComponent', () => {
  let component: PerfilCurrentComponent;
  let fixture: ComponentFixture<PerfilCurrentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerfilCurrentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerfilCurrentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
