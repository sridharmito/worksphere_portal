import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PerfilComponent } from './perfil.component';
import { PerfilResolve } from './service/perfil-resolve.service';
import { PerfilEditComponent } from './perfil-edit/perfil-edit.component';
import { PerfilCurrentComponent } from './perfil-current/perfil-current.component';
import { PerfilChangePassComponent } from './perfil-change-pass/perfil-change-pass.component';

const routes: Routes = [

    {
        path: ':id',
        component: PerfilComponent,
        resolve: { data: PerfilResolve },
        children: [
            {
                path: '',
                component: PerfilCurrentComponent
            },
            {
                path: 'edit',
                component: PerfilEditComponent
            },
            {
                path: 'changePass',
                component: PerfilChangePassComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PerfilRoutingModule { };
