import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PerfilService } from '../service/perfil.service';
import {LoginService} from "../../login/services/login.service";
import {Router} from "@angular/router";

declare var iziToast: any;

@Component({
    selector: 'app-perfil-edit',
    templateUrl: './perfil-edit.component.html',
    styleUrls: ['./perfil-edit.component.css']
})
export class PerfilEditComponent implements OnInit {

    buttonName: string = 'Cancelar'
    currentUser: any;
    perfilForm: FormGroup;
    spinner: boolean;
    showPerfil: false;

    constructor(
        private fb: FormBuilder,
        private perfilService: PerfilService,
        private loginService: LoginService,
        private location: Location,
        private router: Router) { }

    ngOnInit() {
        // CurrentUser
        this.currentUser = JSON.parse(localStorage.getItem('user'));

        this.perfilForm = this.fb.group({
            bio: [this.currentUser['bio'] || ''],
            extra_field1: [this.currentUser['extra_field1'].value || ''],
            extra_field2: [this.currentUser['extra_field2'].value || ''],
            extra_field3: [this.currentUser['extra_field3'].value || ''],
            extra_field4: [this.currentUser['extra_field4'].value || ''],
            extra_field5: [this.currentUser['extra_field5'].value || '']
        });
    }

    back() {
        this.location.back();
    }

    /**
     * Atualiza o perfil do usuário.
     * @param {object} perfil Objeto contendo os dados a serem atualizados
     */
    updatePerfil(perfil) {

        this.spinner = true;

        this.perfilService.updatePerfil(perfil)
            .subscribe(perfil => {

                this.spinner = false;

                // Atualiza o perfil do usuário
                this.loginService.getUserInfo()
                    .subscribe(
                        user => {
                            localStorage.setItem('user', JSON.stringify(user));
                            // Exibe a menssagem de sucesso
                            iziToast.success({
                                title: 'Sucesso',
                                message: 'Seus dados foram atualizado com sucesso.',
                                icon: 'fa fa-info',
                                iconColor: 'green',
                                layout: 2,
                                balloon: true,
                                color: 'white',
                                position: 'bottomCenter',
                                pauseOnHover: false,
                            });

                            this.router.navigate(['/perfil', user.id]);
                        }
                    );
            });
    }

}
