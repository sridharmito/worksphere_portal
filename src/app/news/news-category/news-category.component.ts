import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TitleService } from '../../shared/services/title.service';
import { NewsService } from '../service/news.service';

declare var $: any;

@Component({
    selector: 'app-news-category',
    templateUrl: './news-category.component.html',
    styleUrls: ['./news-category.component.css']
})
export class NewsCategoryComponent implements OnInit {
    loading = true;
    menu: Object;
    allNews: Array<any> = [];
    currentPage: number;
    pageCount: number;

    constructor(
        private route: ActivatedRoute,
        private newsService: NewsService,
        private titleService: TitleService) { }

    ngOnInit() {

        this.titleService.setTitle({ name: 'Noticias', subTitle: true, url: '/home' });

        /**
         * Recebe as noticias do backend;
         * Seta o valor de currentPage e pageCount;
         * Cria um array com as noticias recebidas no primeiro request;
         */
        this.route.data.subscribe(data => {
            this.allNews = [];
            this.pageCount = data['data'].page_count;
            this.currentPage = data['data'].page;
            data['data']._embedded.list.map(elem => {
                this.allNews.push(elem);
            });
            this.loading = false;
        });
    }

    /**
     * Se a pagina chegou no fim, esta função é envocada.
     * Se currentPage < pageCount, é feito um novo request e as noticias recebidas,
     * seram adicionadas no array NEWS atualizando a pagina com as novas noticias.
     * A var currentPage, será incrementada a cada novo request, a fim de previnir requests infinitos.
     */
    onScroll() {
        if ($(window).height() + $(window).scrollTop() == $(document).height()) {
            if(this.currentPage < this.pageCount) {
                let page = this.currentPage +1;
                this.newsService.getNews(page)
                    .subscribe(data => {
                        this.currentPage++;
                        let news = data._embedded.list;
                        data._embedded.list.map(elem => {
                            // Adicona todos as noticias onde slider=false
                            if(!elem['slider']) this.allNews.push(elem);
                        });
                    });
            }
        }
    }

    likeByNews(news: any) {
        news.spinner = true;
        if (news.liked) {
            this.newsService.likeDelCommentById(news['id'])
                .subscribe(like => {
                    news['likes_count']--;
                    news['liked'] = false;
                    news.spinner = false;
                });
        } else {
            this.newsService.likeAddCommentById(news['id'])
                .subscribe(like => {
                    news['likes_count']++
                    news['liked'] = true;
                    news.spinner = false;
                });
        }
    }

}
