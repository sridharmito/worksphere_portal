import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewsComponent } from './news.component';
import { NewsResolve } from './service/news-resolve.service';
import { NewsCategoryComponent } from './news-category/news-category.component';
import { NewsCategoryResolve } from './service/newsCategory-resolve.service';
import { NewsCommentsResolve } from './service/news-comments-resolve.service';

const routes: Routes = [
    {
        path: ':id',
        component: NewsComponent,
        resolve: {
            data: NewsResolve,
            comments: NewsCommentsResolve
        }
    },
    {
        path: 'category/:id',
        component: NewsCategoryComponent,
        resolve: { data: NewsCategoryResolve }
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class NewsRoutingModule { }
