import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { NewsService } from './news.service';

@Injectable()
export class NewsResolve implements Resolve<any> {

    constructor(private newsService: NewsService) { }

    resolve(route: ActivatedRouteSnapshot) {
        let id = route.params['id'];
        return this.newsService.getNewsById(id);
    }
}
