import { TestBed, inject } from '@angular/core/testing';

import { NewsResolveService } from './news-resolve.service';

describe('HomeNewsResolveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NewsResolveService]
    });
  });

  it('should ...', inject([NewsResolveService], (service: NewsResolveService) => {
    expect(service).toBeTruthy();
  }));
});
