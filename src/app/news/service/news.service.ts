import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../../environments/environment';
import { HeadersService } from '../../shared/services/headers.service';
import { HandleErrorService } from '../../shared/services/handle-error.service';

@Injectable()
export class NewsService {

    constructor(
        private http: Http,
        private handleError: HandleErrorService,
        private service: HeadersService) { }

    postFile(fileToUpload: File) {
        const formData: FormData = new FormData();
        formData.append('upload', fileToUpload, fileToUpload.name);
        return this.http
            .post(`${environment.url}/api/file`, formData, { headers: this.service.options() })
            .map((res) => res.json());
    }

    /**
     * Retorna um array com os comentários da notícia.
     * @param {number} id ID da notícia
     */
    getCommentsById(id: number) {
        return this.http.get(`${environment.url}/api/news/${id}/comments`, {headers: this.service.options()})
            .map((res: Response) => res.json());
    }

    /**
     * Retorna a noticia com base no ID enviando.
     * @param {number} id ID da noticia
     */
    getNewsById(id: number) {
        return this.http.get(`${environment.url}/api/news/${id}`, {headers: this.service.options()})
            .map((res: Response) => res.json());
    }

    /**
     * Retorna a noticia por categoria.
     * @param {number} id ID da categoria
     */
    getNewsByCategoryId(id: number) {
        return this.http.get(`${environment.url}/api/news?category_id=${id}`, {headers: this.service.options()})
            .map((res: Response) => res.json());
    }

    /**
     * Cria um novo comtário para uma notícia.
     * @param {number} id ID da notícia
     */
    newComment(id: number, comment: Object) {
        return this.http.post(`${environment.url}/api/news/${id}/comments`, comment, {headers: this.service.options()})
            .map((res: Response) => res.json());
    }

    /**
     * @param {number} id
     * @param comment
     * @returns {Observable<any>}
     */
    updateComment(id: number, comment: any) {
        return this.http.patch(`${environment.url}/api/news/${id}/comments/${comment.id}`, { text: comment.text }, {headers: this.service.options()})
            .map((res: Response) => res.json());
    }

    /**
     * Adiciona um LIKE ao comentário pelo ID da notícia e do comentário.
     * @param {number} newsID ID da notíca
     * @param {number} commentID ID do comentário
     */
    likeAddCommentById(newsID: number) {
        return this.http.post(`${environment.url}/api/news/${newsID}/like`,{}, {headers: this.service.options()})
            .map((res: Response) => res.json());
    }
    likeDelCommentById(newsID: number) {
        return this.http.delete(`${environment.url}/api/news/${newsID}/like`, {headers: this.service.options()})
            .map((res: Response) => res.json());
    }


    /**
     * Remove um LIKE do comentário pelo ID da notícia e do comentário.
     * @param {number} newsID ID da notíca
     * @param {number} commentID ID do comentário
     */
    insertLikeCommentById(newsID: number, commentID) {
        return this.http.post(`${environment.url}/api/news/${newsID}/comments/${commentID}/like`, {}, {headers: this.service.options()})
            .map((res: Response) => res.json());
    }
    removeLikeCommentById(newsID: number, commentID) {
        return this.http.delete(`${environment.url}/api/news/${newsID}/comments/${commentID}/like`, {headers: this.service.options()})
            .map((res: Response) => res.json());
    }

    /**
     * Remove um comentário pelo ID da notícia e do comentário.
     * @param {number} newsID ID da notíca
     * @param {number} commentID ID do comentário
     */
    removeCommentById(newsID: number, commentID: number) {
        return this.http.delete(`${environment.url}/api/news/${newsID}/comments/${commentID}`, {headers: this.service.options()})
            .map((res: Response) => res.json());
    }

    /**
     * Busca as noticias atuais no backend
     * @param {number} page Número da página
     */
    getNews(page: number) {
        return this.http.get(`${environment.url}/api/news?page=${page}`, {headers: this.service.options()})
            .map((res: Response) => res.json())
            .catch(err => Observable.throw(this.handleError.errorToken()));
    }

    fetchSliderNews(max: number) {
        return this.http.get(`${environment.url}/api/news?slider=1&page_size=${max}`, {headers: this.service.options()})
            .map((res: Response) => res.json())
            .catch(err => Observable.throw(this.handleError.errorToken()));
    }

    fetchLatestNews(max: number) {
        return this.http.get(`${environment.url}/api/news?slider=0&page_size=${max}`, {headers: this.service.options()})
            .map((res: Response) => res.json())
            .catch(err => Observable.throw(this.handleError.errorToken()));
    }

    fetchPopularNews(max: number) {
        return this.http.get(`${environment.url}/api/news?slider=0&popular=1&page_size=${max}`, {headers: this.service.options()})
            .map((res: Response) => res.json())
            .catch(err => Observable.throw(this.handleError.errorToken()));
    }

    getNotifications() {
        return this.http.get(`${environment.url}/api/user/me/notifications`, {headers: this.service.options()})
            .map((res: Response) => res.json())
            .catch(err => Observable.throw(this.handleError.errorToken()));
    }

    getOverallSearch(searchText: any) {
        if(searchText){
            return this.http.get(`${environment.url}/api/all-search?search=${searchText}`, {headers: this.service.options()})
            .map((res: Response) => res.json())
            .catch(err => Observable.throw(this.handleError.errorToken()));
        }else{
            return this.http.get(`${environment.url}/api/all-search`, {headers: this.service.options()})
            .map((res: Response) => res.json())
            .catch(err => Observable.throw(this.handleError.errorToken()));
        }
    }

    /**
     * Marca todas as notificações não lidas como lidas.
     */
    setNotificationsAsRead() {
        return this.http.post(`${environment.url}/api/notification/unread/read`, {}, {headers: this.service.options()})
            .map((res: Response) => res.json())
            .catch(err => Observable.throw(this.handleError.errorToken()));
    }

    setNotificationRead(id: string) {
        return this.http.post(`${environment.url}/api/notification/${id}/read`, {}, {headers: this.service.options()})
            .map((res: Response) => res.json())
            .catch(err => Observable.throw(this.handleError.errorToken()));
    }

}
