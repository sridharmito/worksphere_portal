import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { NewsComponent } from './news.component';
import { NewsRoutingModule } from './news.routing';
import { NewsService } from './service/news.service';
import { NewsResolve } from './service/news-resolve.service';
import { NewsCategoryComponent } from './news-category/news-category.component';
import { NewsCategoryResolve } from './service/newsCategory-resolve.service';
import { NewsCommentsResolve } from './service/news-comments-resolve.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NewsRoutingModule,
        ReactiveFormsModule
    ],
    declarations: [
        NewsComponent,
        NewsCategoryComponent
    ],
    providers: [
        NewsService,
        NewsResolve,
        NewsCategoryResolve,
        NewsCommentsResolve
    ]
})
export class NewsModule { }
