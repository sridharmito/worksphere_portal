import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {TitleService} from '../shared/services/title.service';
import {NewsService} from './service/news.service';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
    selector: 'app-news',
    templateUrl: './news.component.html',
    styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
    fileToUpload: File = null;
    comments: Array<any>;
    commentsForm: FormGroup;
    replyForm: FormGroup;
    news: any;
    spinner: boolean;
    userId: string;

    constructor(
        private route: ActivatedRoute,
        private newsService: NewsService,
        private titleService: TitleService,
        private sanitizer: DomSanitizer,
        private fb: FormBuilder) {
        const user = JSON.parse(localStorage.getItem('user'));
        this.userId = user.id;
    }

    ngOnInit() {
        this.route.data.subscribe(data => {
            this.news = data['data'];
            this.news.description = this.sanitizer.bypassSecurityTrustHtml(this.news.description);
        } );
        this.route.data.subscribe(data => {
            this.comments = data['comments']._embedded.list;
            this.comments = this.comments.map((el: any) => {
                el.text = this.sanitizer.bypassSecurityTrustHtml(el.text);
                return el;
            });

        });
        this.titleService.setTitle({ name: 'Noticias', subTitle: true, url: '/home' });

        this.commentsForm = this.fb.group({
            text: ['', Validators.required],
            anon: [''],
            media: ['']
        });

        this.replyForm = this.fb.group({
            text: ['', Validators.required]
        });
    }


    uploadFile(comment) {
        this.newsService.postFile(this.fileToUpload).subscribe((data: any) => {
            console.log(data);
            comment.file_id = data.id;
            this.saveComment(comment);
        }, error => {
            console.log(error);
        });
    }

    newComment(comment) {
        if (this.fileToUpload != null) {
            this.uploadFile(comment);
            this.fileToUpload = null;
        } else {
            this.saveComment(comment);
        }
        this.spinner = true;
    }

    saveComment(comment) {
        console.log(comment);
        this.newsService.newComment(this.news['id'], comment)
            .subscribe(res => {
                this.comments.push(res);
                this.commentsForm.reset();
                this.news.comments_count++;
                this.spinner = false;
            });
    }

    editComment(comment) {
        comment.spinner = true;
        this.newsService.updateComment(this.news.id, { text: comment.text, id: comment.id })
            .subscribe(res => {
                comment.spinner = false;
                comment.editing = false;
            });
    }

    newReply(comment, data) {
        this.spinner = true;
        data.reply_to = comment.id;
        this.newsService.newComment(this.news['id'], data)
            .subscribe(res => {
                comment.replies.push(res);
                comment.replying = false;
                this.replyForm.reset();
                this.spinner = false;
            });
    }

    handleFileInput(files: FileList) {
        this.fileToUpload = files.item(0);
    }

    likes(comment, idx) {
        if (comment.liked) {
            this.newsService.removeLikeCommentById(this.news['id'], comment.id)
                .subscribe(res => {
                    if (comment.reply_to) {
                        comment.likes_count--;
                        comment.liked = false;
                    } else {
                        this.comments[idx]['likes_count']--;
                        this.comments[idx]['liked'] = false;
                    }
                });
        } else {
            this.newsService.insertLikeCommentById(this.news['id'], comment.id)
                .subscribe(res => {
                    if (comment.reply_to) {
                        comment.likes_count++;
                        comment.liked = true;
                    } else {
                        this.comments[idx]['likes_count']++;
                        this.comments[idx]['liked'] = true;
                    }
                });
        }
    }

    likeByNewsId(news) {
        this.spinner = true;
        if (news.liked) {
            this.newsService.likeDelCommentById(news['id'])
                .subscribe(like => {
                    this.news['likes_count']--;
                    this.news['liked'] = false;
                    this.spinner = false;
                });
        } else {
            this.newsService.likeAddCommentById(news['id'])
                .subscribe(like => {
                    this.news['likes_count']++
                     this.news['liked'] = true;
                     this.spinner = false;
                });
        }
    }

    removeComment(comment, idx, parent) {
        console.log('REPLY REMOVE', comment, idx);
        this.newsService.removeCommentById(this.news['id'], comment.id)
            .subscribe(res => {
                if (parent) {
                    parent.replies.splice(idx, 1);
                } else {
                    this.comments.splice(idx, 1);
                    this.news.comments_count--;
                }
            });
    }

    toggleReply(comment) {
        comment.replying = !comment.replying;
    }


}
